import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/views/Homepage'
import ToyLand from '@/views/ToyLand'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Homepage
    },
    {
      path: '/toy',
      name: 'toyLand',
      component: ToyLand
    }
  ]
})
