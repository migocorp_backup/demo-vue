# 演示前端框架 Vue.js

早期混合 HTML, Javascript, CSS 的網頁開發模式，
代碼具有以下缺點: 

- 不易模組化
- 不易維護

於是開始發展出了一些前端框架 

- Angular (Google)
- React.js (Facebook)
- Vue.js

可以用模組化的開發方式，建構前端的代碼。

這裡演示如何 安裝 建立 一個 Vue project，並開始開發一點小功能。

## 開發環境建立

所有前端框架都建構在 node.js 的基礎上，先裝上 node.js。

npm 是 node.js 用來管理 軟件包 (package / library) 的工具。

```bash
# 以 webpack 模板，建立一個名為 wow 的 project 結構:
# 選擇 Runtime + Compiler 
# 選擇 Install vue-router
$ vue init webpack wow

# 進入新建的 wow project, 初始化需要的 package
$ cd wow
$ npm install
$ npm run lint -- --fix

# 等一下會用到 Element-UI, 安裝上
$ npm install element-ui --save

# 啟動開發模式，可以隨改隨看, 訪問 http://localhost:8080
$ npm run dev
```

## 頁面 Homepage

- 可以用 import, 結構化的撰寫程序
- 有自己的 data，連動 HTML template 中的變數
- 有自己的 methods，可被 按鈕 的事件觸發
- link 到 Toyland

## 頁面 ToyLand

- 引用了一個 component : trafficeLight
- 給 component 一個初始值: light="red"
- component 可以控制自己的狀態。
- component 使用 Element-UI, 獲得好看的樣式。
- 重複使用 component : trafficeLight, 兩個紅綠燈互相獨立作業。

## build production files

就是產生 minified js files, index.html, *.css

```
# 建立 production files
$ npm run build
```

## 其他雜事

- [Element-UI](http://element.eleme.io/#/en-US/component/button): 提供了 美觀的，一致的 Desktop Web UI 控件，也有其他的 UI 套件可以選擇。
- [e-charts](http://echarts.baidu.com/examples/) : 提供許多 圖表樣式
- [ECMAScript 6](https://segmentfault.com/a/1190000004365693): 簡稱 ES6, 因為在 2015 年發布，所以也叫做 ECMAScript 2015。ES6 是新一代的 Javascript 語法，基於原來的 javascript, 增加了一些語法。所有前端框架都會用到。
- [Vuex](https://vuex.vuejs.org/zh/guide/): 可以在 整個 SPA(Single Page Application) 中傳遞資料。

## END : 學一個前端框架，向 Full-Stack 邁進！